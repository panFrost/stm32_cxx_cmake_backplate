# /bin/bash
rm -Rf build
cmake -S . -B build -G"Ninja" -DCMAKE_TOOLCHAIN_FILE:PATH="../CMake/arm-none-eabi.cmake"
cmake --build build -j 4