
#include "stm32f7xx_hal.h"

class Output
{
public:
    enum PORT
    {
        GPIO_A,
        GPIO_B,
        GPIO_C,
        GPIO_D,
        GPIO_J
    };
    Output()
    {
    }
    Output(Output::PORT port, int no) : port(port), no(no), counts(0), gpioPort(NULL)
    {

        __HAL_RCC_GPIOJ_CLK_ENABLE();
        GPIO_InitTypeDef gpio;
        gpio.Pin = 1 << no;
        gpio.Mode = GPIO_MODE_OUTPUT_PP;
        gpio.Pull = GPIO_PULLUP;
        switch (port)
        {
        case GPIO_A:
            gpioPort = GPIOA;
            HAL_GPIO_Init(GPIOA, &gpio);
            break;
        case GPIO_B:
            gpioPort = GPIOB;
            HAL_GPIO_Init(GPIOB, &gpio);
            break;
        case GPIO_C:
            gpioPort = GPIOC;
            HAL_GPIO_Init(GPIOC, &gpio);
            break;
        case GPIO_D:
            gpioPort = GPIOD;
            HAL_GPIO_Init(GPIOD, &gpio);
            break;
        case GPIO_J:
            __HAL_RCC_GPIOJ_CLK_ENABLE();
            gpioPort = GPIOJ;
            HAL_GPIO_Init(GPIOJ, &gpio);
            break;
        }
    }
    ~Output()
    {
    }

    void Toggle()
    {
        while (true)
        {
            counts++;
            HAL_GPIO_TogglePin(gpioPort, GPIO_PIN_13);
            HAL_Delay(1000);
        }
    }
    void Set(bool state)
    {
        HAL_GPIO_WritePin(gpioPort, 1 << no, static_cast<GPIO_PinState>(state));
    }
    bool Get()
    {
        return static_cast<bool>(HAL_GPIO_ReadPin(gpioPort, 1 << no));
    }

private:
    Output::PORT port;
    int no;
    int counts;
    GPIO_TypeDef *gpioPort;
    

};
