# stm32_CXX_CMAKE_Backplate

Example project for STM32 platform with C++ and CMake support.
Additionaly support for GitLab CI/CD tools.

# Compilation
Required tools:
 - arm-none-eabi-gcc
 - cmake > 3.0.0

 From build directory call:
  - `cmake .. -G"Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE:PATH="../CMake/arm-none-eabi.cmake"`
  - `make`


# To Do
 - Update .gitlab-ci.yml to get faster image and less unnesesary updates.
 - Support for rest ST platform aside from STM32F7
 - Middleware support (ex FreeRTOS)
 - JLink/STLink support in CMake
 
